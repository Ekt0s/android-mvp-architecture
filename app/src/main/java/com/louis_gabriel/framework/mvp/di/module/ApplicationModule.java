/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.louis_gabriel.framework.mvp.di.module;

import android.app.Application;
import android.content.Context;

import com.louis_gabriel.framework.mvp.BuildConfig;
import com.louis_gabriel.framework.mvp.data.DataManager;
import com.louis_gabriel.framework.mvp.data.db.AppDbHelper;
import com.louis_gabriel.framework.mvp.data.db.DbHelper;
import com.louis_gabriel.framework.mvp.data.network.ApiHeader;
import com.louis_gabriel.framework.mvp.data.network.ApiHelper;
import com.louis_gabriel.framework.mvp.data.network.AppApiHelper;
import com.louis_gabriel.framework.mvp.data.prefs.AppPreferencesHelper;
import com.louis_gabriel.framework.mvp.data.prefs.PreferencesHelper;
import com.louis_gabriel.framework.mvp.di.ApplicationContext;
import com.louis_gabriel.framework.mvp.di.PreferenceInfo;
import com.louis_gabriel.framework.mvp.utils.AppConstants;
import com.louis_gabriel.framework.mvp.data.AppDataManager;
import com.louis_gabriel.framework.mvp.di.ApiInfo;
import com.louis_gabriel.framework.mvp.di.DatabaseInfo;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@ApiInfo String apiKey,
                                                           PreferencesHelper preferencesHelper) {
        return new ApiHeader.ProtectedApiHeader(
                apiKey,
                preferencesHelper.getCurrentUserId(),
                preferencesHelper.getAccessToken());
    }

    @Provides
    @Singleton
    CalligraphyConfig provideCalligraphyDefaultConfig() {
        return new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/source-sans-pro/SourceSansPro-Regular.ttf")
                .setFontAttrId(com.louis_gabriel.framework.mvp.R.attr.fontPath)
                .build();
    }
}
